CREATE TABLE IF NOT EXISTS "doc"."logs" (
    "timestamp" TIMESTAMP NOT NULL,
    "level" STRING NOT NULL,
    "event" STRING NOT NULL,
    "app" STRING NOT NULL,
    "request_id" STRING,
    "trace_id" STRING,
    "user_id" STRING,
    "ip" STRING,
    "data" OBJECT(DYNAMIC)
) WITH (
    column_policy='strict'
);
