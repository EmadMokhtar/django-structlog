from django.apps import AppConfig

from .structlog import request_exception_receiver


class BankConfig(AppConfig):
    name = "bank"

    def ready(self):
        from django.core.signals import got_request_exception

        got_request_exception.connect(request_exception_receiver)
