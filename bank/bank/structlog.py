import datetime
import decimal
import functools
import time
import uuid

import structlog

logger = structlog.get_logger("structlog")


def setup():
    processors = [timestamper, structlog.stdlib.add_log_level, FluentdRenderer()]
    structlog.configure(
        processors=processors,
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=structlog.stdlib.LoggerFactory(),
    )


def request_exception_receiver(sender, request, **kwargs):
    logger.error("request_failed")


def timestamper(logger, log_method, event_dict):
    """
    Add a ``timestamp`` key with the current unix timestamp * 1000
    (CrateDB stores in microseconds) to the event dictionary.
    """
    event_dict["timestamp"] = int(datetime.datetime.utcnow().timestamp() * 1000)
    return event_dict


class FluentdRenderer(structlog.processors.JSONRenderer):
    KEYS = {
        "app",
        "event",
        "ip",
        "level",
        "request_id",
        "timestamp",
        "trace_id",
        "user_id",
    }

    @staticmethod
    def stringify(obj):
        if obj is None:
            return None
        if isinstance(obj, (str, int, float, bool, decimal.Decimal, uuid.UUID)):
            return str(obj)
        if isinstance(obj, (list, tuple, set, frozenset)):
            return [FluentdRenderer.stringify(e) for e in obj]
        if isinstance(obj, dict):
            return {k: FluentdRenderer.stringify(v) for k, v in obj.items()}

        return repr(obj)

    def __call__(self, logger, name, event_dict: dict):
        d = {key: event_dict.pop(key, None) for key in FluentdRenderer.KEYS}
        d["data"] = event_dict
        d = FluentdRenderer.stringify(d)
        return self._dumps(d, **self._dumps_kw)


def middleware(get_response):
    from django.conf import settings

    def _inner(request):
        start_time = time.time()

        request_id = str(uuid.uuid4())
        headers = request.META
        if "HTTP_X_TRACE_ID" in headers:
            trace_id = headers["HTTP_X_TRACE_ID"]
        else:
            trace_id = headers["HTTP_X_TRACE_ID"] = str(uuid.uuid4())
        ip = headers.get("HTTP_X_REAL_IP", headers.get("REMOTE_ADDR"))
        log = logger.new(
            request_id=request_id, ip=ip, trace_id=trace_id, app=settings.BANK_ID
        )

        request.id = request_id
        request.trace_id = trace_id
        log.debug("request_started", path=request.path)
        response = get_response(request)
        end_time = time.time()
        log.debug(
            "request_finished",
            request_time_millis=int((end_time - start_time) * 1000),
            status_code=response.status_code,
        )
        return response

    return _inner


def end_middleware(get_response):
    def _inner(request):
        if request.user.is_authenticated:
            logger.bind(user_id=request.user.pk)
        return get_response(request)

    return _inner


def format_task_name(task):
    if isinstance(task, dict) and "task" in task:
        return task["task"]
    return f"{task.__module__}.{task.__name__}"


def task_looger(func):
    from django.conf import settings

    @functools.wraps(func)
    def _wrapper(*args, **kwargs):
        trace_id = kwargs["trace_id"]
        task_name = format_task_name(func)
        log = logger.new(trace_id=trace_id, app=settings.BANK_ID, task_name=task_name)
        log.debug("task_started")
        start_time = time.time()
        result = func(log, *args, **kwargs)
        end_time = time.time()
        log.debug("task_finished", task_time_millis=int((end_time - start_time) * 1000))
        return result

    return _wrapper
