"""bank URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from django.views.generic import RedirectView

from .views import (
    CashCheckView,
    CheckbookView,
    CustomerView,
    DepositView,
    NewChecksView,
    NotificationView,
    RegisterView,
    RemoteTransferView,
    TransferView,
    WithdrawView,
    add_notification_api,
    bank_to_bank_deposit,
    route_check_api,
)

urlpatterns = [
    path(
        "customer/login/",
        LoginView.as_view(template_name="bank/login.html"),
        name="login",
    ),
    path("customer/logout/", LogoutView.as_view(), name="logout"),
    path("customer/register/", RegisterView.as_view(), name="register"),
    path("customer/", CustomerView.as_view(), name="customer"),
    path("checks/", CheckbookView.as_view(), name="checkbook"),
    path("checks/new/", NewChecksView.as_view(), name="new-checks"),
    path("checks/cash/", CashCheckView.as_view(), name="cash-check"),
    path("account/deposit/", DepositView.as_view(), name="deposit"),
    path("account/transfer/", TransferView.as_view(), name="transfer"),
    path(
        "account/transfer/remote/", RemoteTransferView.as_view(), name="transfer-remote"
    ),
    path("account/withdraw/", WithdrawView.as_view(), name="withdraw"),
    path("notifications/", NotificationView.as_view(), name="notifications"),
    path("api/bank_to_bank_deposit/", bank_to_bank_deposit),
    path("api/route_check/", route_check_api),
    path("api/add_notification/", add_notification_api),
    path("", RedirectView.as_view(pattern_name="customer"), name="index"),
]
