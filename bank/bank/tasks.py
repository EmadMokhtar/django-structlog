import decimal

import requests
from celery import shared_task, signature
from django.conf import settings
from django.db import transaction

from .models import PP_MAP_INVERSE, Account, Check, InsufficientFunds
from .structlog import format_task_name, task_looger


def send_request(*, bank, path, trace_id, **data):
    headers = {"X-Auth-PSK": settings.DJANGO_BANK_PSK, "X-Trace-ID": trace_id}
    url = f"http://{bank}:8000{path}"
    response = requests.post(url, json=data, headers=headers)
    return response


def get_task(task):
    if isinstance(task, dict):
        return signature(task["task"], args=task["args"], kwargs=task["kwargs"])
    return task


def log_and_handle_api_error(
    response, logger, event, error_callback=None, error_callback_options=None
):
    response_data = None
    if response.status_code in {200, 400, 401}:
        response_data = response.json()
    log_data = {
        "response_status_code": response.status_code,
        "response_data": response_data,
    }
    if error_callback:
        log_data["scheduled_task_name"] = (format_task_name(error_callback),)
        logger.error(event, **log_data)
        get_task(error_callback).apply_async(**(error_callback_options or {}))
    else:
        logger.error(event, **log_data)


@shared_task
@task_looger
def transfer(
    logger,
    *,
    target_bank,
    from_account_id,
    to_account_id,
    amount,
    trace_id,
    error_callback=None,
):
    logger.bind(
        from_account=from_account_id,
        target_bank=target_bank,
        to_account=to_account_id,
        amount=amount,
    )
    try:
        with transaction.atomic():
            Account.objects.withdraw(from_account_id, decimal.Decimal(amount))
            response = send_request(
                bank=target_bank,
                path="/api/bank_to_bank_deposit/",
                trace_id=trace_id,
                account_id=to_account_id,
                amount=amount,
            )
            response.raise_for_status()
    except requests.HTTPError:
        log_and_handle_api_error(
            response, logger, "money_transfer_failed", error_callback
        )


@shared_task
@task_looger
def route_check(
    logger, *, aba_rtn, target_bank, to_account_id, amount, trace_id, error_callback
):
    logger = logger.bind(
        aba_rtn=aba_rtn,
        amount=amount,
        target_bank=target_bank,
        to_account=to_account_id,
    )
    check_bank = PP_MAP_INVERSE[aba_rtn[:2]]
    if check_bank == settings.BANK_ID:
        # Check is from this bank.
        logger.info("check_cash")
        try:
            with transaction.atomic():
                check = (
                    Check.objects.select_related("customer__account")
                    .select_for_update(of=["self"])
                    .get(aba_rtn=aba_rtn, used=False)
                )
                if (
                    check.customer.account.id == to_account_id
                    and check_bank == target_bank
                ):
                    # Same customer. Reject check. Noop, but log event
                    logger.warning("check_cash_same_user")
                    return
                check.used = True
                check.save(update_fields=["used"])

                Account.objects.withdraw(
                    check.customer.account.id, decimal.Decimal(amount)
                )
                response = send_request(
                    bank=target_bank,
                    path="/api/bank_to_bank_deposit/",
                    trace_id=trace_id,
                    account_id=to_account_id,
                    amount=amount,
                )
                response.raise_for_status()
        except Check.DoesNotExist:
            logger.error(
                "check_unknown", scheduled_task_name=format_task_name(error_callback)
            )
            error_callback["kwargs"]["message"].append("Check unknown.")
            get_task(error_callback).apply_async(countdown=settings.CHECK_ROUTING_DELAY)
        except InsufficientFunds:
            logger.error(
                "check_not_secured",
                scheduled_task_name=format_task_name(error_callback),
            )
            error_callback["kwargs"]["message"].append(
                "Check not secured. Insufficient funds."
            )
            get_task(error_callback).apply_async(countdown=settings.CHECK_ROUTING_DELAY)
        except requests.HTTPError:
            error_callback["kwargs"]["message"].append(
                "Repositing check into your account failed."
            )
            log_and_handle_api_error(
                response,
                logger,
                "check_cash_failed",
                error_callback,
                {"countdown": settings.CHECK_ROUTING_DELAY},
            )
    else:
        # Check is from another bank. Routing it there.
        logger.info("routing_check")
        try:
            response = send_request(
                bank=check_bank,
                path="/api/route_check/",
                trace_id=trace_id,
                aba_rtn=aba_rtn,
                target_bank=target_bank,
                to_account_id=to_account_id,
                amount=amount,
                error_callback=error_callback,
            )
            response.raise_for_status()
        except requests.HTTPError:
            error_callback["kwargs"]["message"].append("Routing to target bank failed.")
            log_and_handle_api_error(
                response,
                logger,
                "check_routing_failed",
                error_callback,
                {"countdown": settings.CHECK_ROUTING_DELAY},
            )


@shared_task
@task_looger
def add_notification(logger, *, target_bank, account, message, trace_id):
    send_request(
        bank=target_bank,
        path="/api/add_notification/",
        trace_id=trace_id,
        account=account,
        message=message,
    )
