import decimal
import json

import structlog
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import Form
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.crypto import constant_time_compare
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import CreateView, DetailView, FormView, ListView

from .forms import (
    DepositWithdrawForm,
    RegisterForm,
    RemoteTransferForm,
    TransferForm,
    CashCheckForm,
)
from .models import Account, Check, Customer, InsufficientFunds, Notification
from .structlog import format_task_name
from .tasks import add_notification, route_check, transfer

logger = structlog.get_logger("structlog")


class ForCustomerMixin:
    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(customer=self.request.user)
        return qs


class CustomerView(LoginRequiredMixin, DetailView):
    template_name = "bank/customer.html"

    def get_object(self):
        return self.request.user


class RegisterView(CreateView):
    model = Customer
    form_class = RegisterForm
    template_name = "bank/register.html"
    success_url = reverse_lazy("customer")


class DepositView(LoginRequiredMixin, FormView):
    template_name = "bank/deposit.html"
    form_class = DepositWithdrawForm
    success_url = reverse_lazy("customer")

    def form_valid(self, form):
        amount = form.cleaned_data["amount"]
        Account.objects.deposit(self.request.user.account.id, amount)
        return super().form_valid(form)


@require_POST
@csrf_exempt
def bank_to_bank_deposit(request):
    psk = request.META["HTTP_X_AUTH_PSK"]
    if not constant_time_compare(psk, settings.DJANGO_BANK_PSK):
        logger.warning("invalid_psk")
        return JsonResponse({"error": "invalid pre shared key"}, status=401)

    data = json.loads(request.body)
    account_id = data["account_id"]
    amount = decimal.Decimal(data["amount"])
    try:
        Account.objects.deposit(account_id, amount)
    except Account.DoesNotExist:
        return JsonResponse({"error": "Account unknown"}, status=400)
    return JsonResponse({"status": "ok"}, status=200)


class TransferView(LoginRequiredMixin, FormView):
    template_name = "bank/transfer.html"
    form_class = TransferForm
    success_url = reverse_lazy("customer")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["current_customer"] = self.request.user
        return kwargs

    def form_valid(self, form):
        from_account_id = self.request.user.account.id
        to_account_id = form.cleaned_data["account_id"]
        amount = form.cleaned_data["amount"]
        try:
            Account.objects.transfer(from_account_id, to_account_id, amount)
        except InsufficientFunds as e:
            form.add_error("amount", str(e))
            return self.form_invalid(form)
        return super().form_valid(form)


class RemoteTransferView(LoginRequiredMixin, FormView):
    template_name = "bank/transfer_remote.html"
    form_class = RemoteTransferForm
    success_url = reverse_lazy("customer")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["current_customer"] = self.request.user
        return kwargs

    def form_valid(self, form):
        from_account_id = self.request.user.account.id
        to_account_id = form.cleaned_data["account_id"]
        amount = form.cleaned_data["amount"]
        bank = form.cleaned_data["bank"]
        logger.info(
            "money_transfer_scheduled",
            target_bank=bank,
            from_account=from_account_id,
            to_account=to_account_id,
            amount=amount,
            scheduled_task_name=format_task_name(transfer),
        )
        error_callback = add_notification.signature(
            kwargs={
                "target_bank": settings.BANK_ID,
                "account": from_account_id,
                "message": [f"Sending {amount} to {to_account_id} at {bank} failed."],
                "trace_id": self.request.trace_id,
            }
        )
        transfer.delay(
            target_bank=bank,
            from_account_id=from_account_id,
            to_account_id=to_account_id,
            amount=amount,
            trace_id=self.request.trace_id,
            error_callback=error_callback,
        )
        return super().form_valid(form)


class WithdrawView(LoginRequiredMixin, FormView):
    template_name = "bank/withdraw.html"
    form_class = DepositWithdrawForm
    success_url = reverse_lazy("customer")

    def form_valid(self, form):
        amount = form.cleaned_data["amount"]
        try:
            Account.objects.withdraw(self.request.user.account.id, amount)
        except InsufficientFunds as e:
            form.add_error("amount", str(e))
            return self.form_invalid(form)
        return super().form_valid(form)


class NotificationView(LoginRequiredMixin, ForCustomerMixin, ListView):
    model = Notification
    template_name = "bank/notification.html"


class CheckbookView(LoginRequiredMixin, ForCustomerMixin, ListView):
    model = Check
    template_name = "bank/checkbook.html"


class NewChecksView(LoginRequiredMixin, FormView):
    form_class = Form
    template_name = "bank/new_checks.html"
    success_url = reverse_lazy("checkbook")

    def form_valid(self, form):
        Check.objects.new_checkbook(self.request.user)
        return super().form_valid(form)


class CashCheckView(LoginRequiredMixin, FormView):
    template_name = "bank/cash_check.html"
    form_class = CashCheckForm
    success_url = reverse_lazy("customer")

    def form_valid(self, form):
        to_account_id = self.request.user.account.id
        aba_rtn = form.cleaned_data["aba_rtn"]
        amount = form.cleaned_data["amount"]

        logger.info(
            "check_routing_scheduled",
            aba_rtn=aba_rtn,
            target_bank=settings.BANK_ID,
            to_account=to_account_id,
            amount=amount,
            scheduled_task_name=format_task_name(route_check),
        )
        error_callback = add_notification.signature(
            kwargs={
                "target_bank": settings.BANK_ID,
                "account": to_account_id,
                "message": [f"Cashing check {aba_rtn} with amount {amount} failed."],
                "trace_id": self.request.trace_id,
            }
        )
        route_check.apply_async(
            kwargs={
                "aba_rtn": aba_rtn,
                "target_bank": settings.BANK_ID,
                "to_account_id": to_account_id,
                "amount": amount,
                "trace_id": self.request.trace_id,
                "error_callback": error_callback,
            },
            countdown=settings.CHECK_ROUTING_DELAY,
        )
        return super().form_valid(form)


@require_POST
@csrf_exempt
def route_check_api(request):
    psk = request.META["HTTP_X_AUTH_PSK"]
    if not constant_time_compare(psk, settings.DJANGO_BANK_PSK):
        logger.warning("invalid_psk")
        return JsonResponse({"error": "invalid pre shared key"}, status=401)

    data = json.loads(request.body)
    logger.info(
        "check_routing_scheduled",
        aba_rtn=data["aba_rtn"],
        target_bank=data["target_bank"],
        to_account=data["to_account_id"],
        amount=data["amount"],
        scheduled_task_name=format_task_name(route_check),
    )
    route_check.apply_async(
        kwargs={
            "aba_rtn": data["aba_rtn"],
            "target_bank": data["target_bank"],
            "to_account_id": data["to_account_id"],
            "amount": data["amount"],
            "trace_id": request.trace_id,
            "error_callback": data["error_callback"],
        },
        countdown=settings.CHECK_ROUTING_DELAY,
    )
    return JsonResponse({"status": "ok"}, status=200)


@require_POST
@csrf_exempt
def add_notification_api(request):
    psk = request.META["HTTP_X_AUTH_PSK"]
    if not constant_time_compare(psk, settings.DJANGO_BANK_PSK):
        logger.warning("invalid_psk")
        return JsonResponse({"error": "invalid pre shared key"}, status=401)

    data = json.loads(request.body)
    Notification.new(
        account=data["account"], message=data["message"], trace_id=request.trace_id
    )

    return JsonResponse({"status": "ok"}, status=200)
