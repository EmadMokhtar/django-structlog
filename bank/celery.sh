#!/bin/sh

WAIT=2
echo "Waiting $WAIT seconds for databases to be there ..."
sleep $WAIT
exec celery -A bank worker -l debug
