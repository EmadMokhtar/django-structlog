# django-structlog

This project demonstrates how one can use structured logging in Django.

## Installation

This project requires [docker-compose](https://docs.docker.com/compose/). When
``docker-compose`` is installed, one needs to setup the initial databases:

```shell
$ docker-compose up --detach cratedb postgres
```

Both services take a short while to start up. After a few seconds you should be
able to see the following output when you run `docker-compose ps`:
```shell
$ docker-compose ps
           Name                          Command              State                     Ports                   
----------------------------------------------------------------------------------------------------------------
django-structlog_cratedb_1    /docker-entrypoint.sh crate     Up      0.0.0.0:4200->4200/tcp, 4300/tcp, 5432/tcp
django-structlog_postgres_1   docker-entrypoint.sh postgres   Up      5432/tcp
```

Then follows the bootstrapping. First, there needs to be a database table to
store the log data:

```bash
$ docker-compose exec cratedb sh -c 'crash < /logs.sql'
```

Second, we create some PostgreSQL users and databases:

```bash
$ docker-compose exec postgres sh -c 'psql -U postgres -f /bootstrap.sql'
```

Lastly, we can spin up the entire project with this command:

```shell
$ docker-compose up --detach
```

This will set up these services:
- A few Django applications
- An NGINX reverse proxy to serve all web requests
- A Redis service as a message broker
- A PostgreSQL database as a database used by the applications
- A CrateDB cluster as long-term log storage
- A Fluentd deamon to fetch structured logs from the applications and push them into CrateDB
- A Grafana dashboard to visualize the logs

You can then visit http://localhost:8000/
