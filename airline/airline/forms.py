import datetime

import arrow
import requests
import structlog
from django import forms
from django.conf import settings

from .models import Traveller, Booking

logger = structlog.get_logger("structlog")


class RegisterForm(forms.ModelForm):
    class Meta:
        model = Traveller
        fields = ("username", "password", "name", "dob")
        widgets = {"password": forms.PasswordInput}

    def save(self, *args, **kwargs):
        cd = self.cleaned_data
        traveller = Traveller.objects.create(
            cd["username"], cd["password"], cd["name"], cd["dob"]
        )
        logger.bind(user_id=traveller.id)
        logger.info("new_traveller", user_id=traveller.id)
        return traveller


class NewBookingForm(forms.ModelForm):

    departure_date = forms.DateField()

    class Meta:
        model = Booking
        fields = ("travellers", "flight")

    def save(self, commit=True):
        flight = self.instance.flight
        departure = arrow.get(
            datetime.datetime.combine(
                self.cleaned_data["departure_date"], flight.departure_time
            ),
            flight.departure_tz,
        )
        arrival = (departure + flight.duration).to(flight.arrival_tz)
        self.instance.departure_date = departure.datetime
        self.instance.arrival_date = arrival.datetime
        return super().save(commit=commit)


PAYMENT_OPTION_WIRE = "wire"
PAYMENT_OPTION_CHECK = "check"
PAYMENT_OPTION_CHOICES = (
    (PAYMENT_OPTION_WIRE, "Wire transfer"),
    (PAYMENT_OPTION_CHECK, "Check deposit"),
)


class NewBookingPaymentForm(forms.Form):
    payment_option = forms.ChoiceField(choices=PAYMENT_OPTION_CHOICES, required=True)

    wire_bank = forms.ChoiceField(
        required=False, choices=(("westbank", "westbank"), ("eastbank", "eastbank"))
    )
    wire_username = forms.CharField(required=False)
    wire_password = forms.CharField(required=False, widget=forms.PasswordInput)

    check_number = forms.CharField(required=False)

    def clean(self):
        cd = self.cleaned_data
        if cd["payment_option"] == PAYMENT_OPTION_CHECK:
            if "check_number" not in cd:
                self.add_error(
                    "check_number", self.check_number.error_messages["required"]
                )
        elif cd["payment_option"] == PAYMENT_OPTION_WIRE:
            if "wire_bank" not in cd:
                self.add_error("wire_bank", self.wire_bank.error_messages["required"])
            if "wire_username" not in cd:
                self.add_error(
                    "wire_username", self.wire_username.error_messages["required"]
                )
            if "wire_password" not in cd:
                self.add_error(
                    "wire_password", self.wire_password.error_messages["required"]
                )
        return self.cleaned_data
