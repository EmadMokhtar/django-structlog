import requests
import structlog
from django.conf import settings

from .forms import PAYMENT_OPTION_CHECK

logger = structlog.get_logger("structlog")


class PaymentClient:
    def __init__(self, booking, trace_id):
        self.booking = booking
        self.trace_id = trace_id
        self._client = requests.Session()
        self._client.headers["X-Trace-ID"] = self.trace_id

    def process(self, payment_option, **payment_data):
        logger.bind(payment_option=payment_option)
        if payment_option == PAYMENT_OPTION_CHECK:
            self.process_check(self.booking.flight.price, **payment_data)
        else:
            self.process_wire_transfer(self.booking.flight.price, **payment_data)

    def process_check(self, price, **payment_data):
        url = f"{settings.WESTBANK_HOST}/customer/login/"
        response = self._client.get(url)
        self._handle_error(response, url, "login_unavailable")
        response = self._client.post(
            url,
            data={
                "csrfmiddlewaretoken": self._client.cookies["csrftoken"],
                "username": settings.WESTBANK_CREDENTIALS_USERNAME,
                "password": settings.WESTBANK_CREDENTIALS_PASSWORD,
            },
            allow_redirects=False,
        )
        self._handle_error(response, url, "login_failed", status_code=302)

        url = f"{settings.WESTBANK_HOST}/checks/cash/"
        response = self._client.get(url)
        self._handle_error(response, url, "cash_check_unavailable")
        response = self._client.post(
            url,
            data={
                "csrfmiddlewaretoken": self._client.cookies["csrftoken"],
                "aba_rtn": payment_data["check_number"],
                "amount": str(price),
            },
            allow_redirects=False,
        )
        self._handle_error(response, url, "cash_check_failed", status_code=302)

    def process_wire_transfer(self, price, **payment_data):
        host = f"http://{payment_data['wire_bank']}:8000"

        url = f"{host}/customer/login/"
        response = self._client.get(url)
        self._handle_error(response, url, "login_unavailable")
        response = self._client.post(
            url,
            data={
                "csrfmiddlewaretoken": self._client.cookies["csrftoken"],
                "username": payment_data["wire_username"],
                "password": payment_data["wire_password"],
            },
            allow_redirects=False,
        )
        self._handle_error(response, url, "login_failed", status_code=302)

        url = f"{host}/account/transfer/remote/"
        response = self._client.get(url)
        self._handle_error(response, url, "wire_transfer_unavailable")
        response = self._client.post(
            url,
            data={
                "csrfmiddlewaretoken": self._client.cookies["csrftoken"],
                "account_id": settings.WESTBANK_CREDENTIALS_ACCOUNT_ID,
                "amount": str(price),
                "bank": "westbank",
            },
            allow_redirects=False,
        )
        self._handle_error(response, url, "wire_transfer_failed", status_code=302)

    def _handle_error(self, response, url, msg, status_code=200):
        if response.status_code != status_code:
            logger.error(
                "payment_failed",
                message=msg,
                payment_process_url=url,
                response_status_code=response.status_code,
            )
            print(response.content.decode())
            raise requests.HTTPError()
