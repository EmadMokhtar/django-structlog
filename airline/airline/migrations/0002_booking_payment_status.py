# Generated by Django 2.1.7 on 2019-03-24 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("airline", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="booking",
            name="payment_status",
            field=models.CharField(
                choices=[("open", "Open"), ("pending", "Pending"), ("done", "Done")],
                default="open",
                max_length=7,
            ),
        )
    ]
